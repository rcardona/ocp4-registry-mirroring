OCP_RELEASE='4.10.3'
LOCAL_REGISTRY='bastion.ssalab.nl:5000'
LOCAL_REPOSITORY='ocp4/openshift4'
PRODUCT_REPO='openshift-release-dev'
LOCAL_SECRET_JSON='${HOME}/bundle-pullsecret.txt'
RELEASE_NAME="ocp-release"
ARCHITECTURE="x86_64"
OPENSHIFT_INSTALL="/home/ec2-user/artifacts-4-10-3/openshift-install"


/home/ec2-user/artifacts-4-10-3/oc adm release extract -a ${LOCAL_SECRET_JSON} --command=${OPENSHIFT_INSTALL} "${LOCAL_REGISTRY}/${LOCAL_REPOSITORY}:${OCP_RELEASE}"

/home/ec2-user/artifacts-4-10-3/oc adm release extract -a ${LOCAL_SECRET_JSON} --command=${OPENSHIFT_INSTALL} "${LOCAL_REGISTRY}/${LOCAL_REPOSITORY}:${OCP_RELEASE}-${ARCHITECTURE}"
