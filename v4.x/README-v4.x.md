# Registry Mirroring Openshift v4.x

__The procedure has been tested with Openshift Enterprise Version 4.10.13 on linux/amd64 only (yet)__

__Objective__

The goal of this procedure is to automate the provisioning of a local registry, in order to host the container images needed for the deployment and operations of an Openshift Enterprise v4.x cluster.

__User Cases__

Using local container images registries is a common setup that allows organizations to provision and operate Openshift clusters in highly secure environments, where online connectivity is restricted, or simply disconnected environments where internet access is hindered by geographical constrains. Here below there are two specific use cases:

- *USE CASE 1. Provision mirrored local registry for a connected environment*: the local registry has access to the public Red Hat registries.

- *USE CASE 1. Provision mirrored local registry for a disconnected environment*: the local registry has no access to internet neither public Red Hat registries.


__Concepts__

Each of the use cases has two main steps; the first step (Provisioning Local Registry) refers to provisioning the local registry and mirroring of the container images (aka base images for a vanilla OCP deployment). The second step (Creating Operator Catalogs) refers to creating Operators catalogs and mirroring the Operators (aka custom Operators Catalogs).

__Understanding Operator Catalogs__

An Operator catalog is a repository of metadata that Operator Lifecycle Manager (OLM) uses to query, discover and install Operators and their dependencies on a cluster. OLM always installs Operators from the latest version of a catalog. Red Hat-provided catalogs are distributed using index images.

An index image, based on the Operator Bundle Format, is a containerized snapshot of a catalog. It is an immutable artifact that contains the database of pointers to a set of Operator manifest content. A catalog can reference an index image to source its content for OLM on the cluster.


## USE CASES


__USE CASE 1__. Provision mirrored local registry for a connected environment. Here the registry has access to the public Red Hat container registries for the mirroring of the container images.


### 1.1. Provisioning Local Registry

> wget https://gitlab.com/rcardona/ocp4-registry-mirroring/-/raw/master/v4.x/mirror-registry-v4.x.sh
>

How to provision the local registry

`Registry User: admin`

`Password: admin`

![Provisioning Local Registry](media/basic-registry.mp4)


### 1.2. Creating Operator Catalogs

#### 1.2.1. Creating Default Catalogs

- Prerequisites

  * Workstation with network access to public Red Hat registries

  * Access to a registry that supports Docker v2-2 (local registry)

- Authenticate to registry.redhat.io
> podman login registry.redhat.io

- Authenticate to local registry registry:
>
> podman login <REGISTRY_FQDN>:<REGISTRY_PORT>

- There are four default Operator Catalogs (organizations); `redhat-operators`, `certified-operators`, `redhat-marketplace` and `community-operators`. Command to mirror the `redhat-operators` organization: `mirror-registry-v47.sh <ORGANIZATION>`

Ex.
>
> ./mirror-registry-v47.sh redhat-operators
>

Creating Default Operator Catalogs

![Creating Default Operator Catalogs](media/base.mp4)

---

#### 1.2.2. Creating Custom Catalogs

__Pruning an index image__ (removing unneeded content) is the way to customize the images that mirror the Operators packages. It helps to remove the metadata of the images that are not meant to be mirrored; therefore saving storage and shorting elapsing time.

- Prerequisites

  * Workstation with network access to public Red Hat registries

  * Access to a registry that supports Docker v2-2 (local registry)

- Authenticate to registry.redhat.io
> podman login registry.redhat.io

- Authenticate to local registry registry:
>
> podman login <REGISTRY_FQDN>:<REGISTRY_PORT>

- List the images that are meant to be mirrored from the default Operator Catalogs. Here there are the four default Operator Catalogs (organizations); `redhat-operators`, `certified-operators`, `redhat-marketplace` and `community-operators`. Command to list images per organization catalog: `mirror-registry-v47.sh list_<ORGANIZATION>`

Ex.
>
> ./mirror-registry-v47.sh list_redhat-operators
>


- Once the needed Operators are chosen, then the custom Operator catalog has be to created. This step will:

  - Prune the organization container image that will mirror (only) the mentioned operators packages.
  - Push that image to the local registry.
  - Create the custom Operator catalog on the local registry.

Let's assume that only two Operators has been chosen (jaeger-product and quay-operator) in the "redhat-operator" organization operators catalog. Command to create custom Operator catalogs:  `mirror-registry-v47.sh create-custom-catalog-<ORGANIZATION> [OPERATOR_NAME_1, ...  ,OPERATOR_NAME_N]`

Ex.
>
> ./mirror-registry-v47.sh create-custom-catalog-redhat-operator jaeger-product,quay-operator
>

Creating Custom Operator Catalogs

![Creating Custom Operator Catalogs](media/custom-catalogs.mp4)

---

__USE CASE 2__. Provision mirrored local registry for a disconnected environment. Here the registry has no access to internet, so  it is not possible to have direct access to the public Red Hat container registries. There the mirroring shall take place from a portable media, where all the necessary container images has been previously mirrored.

#### 2.1. Mirroring installation container images to portable media

- Download and update the mirroring script. __This step shall be done on a host with direct access to the public Red Hat registries__.

> wget https://gitlab.com/rcardona/ocp4-registry-mirroring/-/raw/master/v4.7/mirror-registry-v47.sh
>

- Mirror OCP base installation images to a portable file in order to bring it to the disconnected environment.

> ./mirror-registry-v47.sh export-base-registry
>

This will generate a file (Ex. /home/raf/mirror-base-4.7.2.tar.gz).

Ex.
> ls -sh xzvf mirror-base-4.7.2.tar.gz
>

#### 2.2. Provisioning Local Registry in the Disconnected Environment

- Download and update the mirroring script. __This step shall be done on the server hosting the local registry in the disconnected environment__.

      sudo podman pull quay.io/redhat-emea-ssa-team/registry:2
      sudo podman save -o redhat-emea-ssa-team-registry.tar --format oci-archive quay.io/redhat-emea-ssa-team/registry:2
      sudo podman pull oci-archive:redhat-emea-ssa-team-registry.tar
      podman tag 09d35e299d31 redhat-emea-ssa-team/registry:2

> wget https://gitlab.com/rcardona/ocp4-registry-mirroring/-/raw/master/v4.7/mirror-registry-v47.sh
>
> ./mirror-registry-v47.sh prep_registry

#### 2.3. Uploading Container Base Images to the Local Registry

- Move the tar file with the container images (Ex. /home/raf/mirror-base-4.7.2.tar.gz) to the local registry in the disconnected environment, and decompress it.

Ex.
> tar xzvf mirror-base-4.7.2.tar.gz
>

- Upload the base container images to the local registry. Command: `oc image mirror  -a ${HOME}/bundle-pullsecret.txt --from-dir=./mirror-base-${OCP_RELEASE} "file://openshift/release:${OCP_RELEASE}*" ${REGISTRY_FQDN}:${REGISTRY_PORT}/${LOCAL_REPOSITORY}`

> oc image mirror  -a /home/raf/bundle-pullsecret.txt --from-dir=./mirror-base-4.7.2 "file://openshift/release:4.7.2*" control.doma.casa:5000/ocp4/openshift4 --insecure
>

#### 2.4. Mirror Default Operators Catalogs to the disconnected environments

__NOTE: THIS STEP WILL CREATE BE A BIG SIZE FILE, APPROX 260 GB. PLEASE CONSIDER MIRRORING ONLY THE NEEDED OPERATOR PACKAGES WITH CUSTOM OPERATOR CATALOGS, STEP 2.5__

- Mirror default operator catalogs to a portable file. Command: `oc adm catalog mirror -a ${HOME}/bundle-pullsecret.txt registry.redhat.io/redhat/${ORGANIZATION}-index:v4.7 file://mirror-${ORGANIZATION}-${OCP_RELEASE} --insecure`

Example of __*redhat-operator*__ catalog organization

> oc adm catalog mirror -a /home/raf/bundle-pullsecret.txt registry.redhat.io/redhat/redhat-operator-index:v4.7 file://mirror-redhat-operators-4.7.2 --insecure
>

- It will create a file containing the operators (Ex. mirror-redhat-operators-4.7.2.tar.gz)

> ls -sh mirror-redhat-operators-4.7.2.tar.gz

- Move the tar file with the operator container images to the local registry in the disconnected environment, and decompress it.

Ex.
> tar xzvf mirror-redhat-operators-4.7.2.tar.gz
>

- Upload the operator container images to the local registry. Command: `oc adm catalog mirror -a ${HOME}/bundle-pullsecret.txt registry.redhat.io/redhat/redhat-operator-index:v4.7 file://mirror-${ORGANIZATION}-${OCP_RELEASE} --insecure`

Ex.
> oc adm catalog mirror -a /home/raf/bundle-pullsecret.txt file://mirror-redhat-operators-4.7.2/redhat/redhat-operator-index:v4.7 control.doma.casa:5000/olm-redhat-operators --insecure
>








#### 2.5. Mirror Custom Operators Catalogs to the disconnected environments

- Define the operators packages that will be mirrored to the local registry in the disconnected environment. On host that has direct access to the public Red Hat registries execute the following command:

> export OPERATOR_NAMES="ocs-operator,rhsso-operator"
>

Ex. Let's assume that only two Operators has been chosen (ocs-operator and rhsso-operator) in the "redhat-operator" organization operators catalog. Command to mirror custom Operator catalogs:  `mirror-registry-v47.sh mirror-custom-catalog-<ORGANIZATION> [OPERATOR_NAME_1, ...  ,OPERATOR_NAME_N]`

> mirror-registry-v47.sh mirror-custom-catalog-redhat-operators ocs-operator,rhsso-operator
>

- The previous step (if it is executed successfully) will generate a file as follows (${HOME}/mirror-${ORGANIZATION}-${OCP_RELEASE}.tar.gz):

> ls -sh /home/raf/mirror-redhat-operators-4.7.2.tar.gz
>

- Move the tar file with the custom operator catalog to the local registry in the disconnected environment, and decompress it.

Ex.
> tar xzvf mirror-redhat-operators-4.7.2.tar.gz
>

- Upload the operator container images to the local registry. Command: `oc adm catalog mirror -a ${HOME}/bundle-pullsecret.txt file://mirror-redhat-operators-${OCP_RELEASE}/olm-redhat-operators/redhat-operator-index:v4.7 ${REGISTRY_FQDN}:${REGISTRY_PORT}/olm-${ORGANIZATION} --insecure`

Ex.
> oc adm catalog mirror -a /home/raf/bundle-pullsecret.txt file://mirror-redhat-operators-4.7.2/olm-redhat-operators/redhat-operator-index:v4.7 control.doma.casa:5000/olm-redhat-operators --insecure
>


# LAST BUT NOT LEAST
In order to enabling the mirrored registries in a disconnected cluster, it needs to update the OperatorHub sources, look at [here](https://docs.openshift.com/container-platform/4.7/operators/admin/olm-restricted-networks.html) for more information.

- installing catalogSource


> oc create -f <path/to/manifests/dir>/catalogSource.yaml
>

> oc create -f <path/to/manifests/dir>/imageContentSourcePolicy.yaml
>
