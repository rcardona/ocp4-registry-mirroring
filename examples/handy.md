> cd /var/lib/libvirt/images/
>
> wget https://cloud.centos.org/centos/8/x86_64/images/CentOS-8-GenericCloud-8.3.2011-20201204.2.x86_64.qcow2
>
> qemu-img create -f qcow2 -b /var/lib/libvirt/images/CentOS-8-GenericCloud-8.3.2011-20201204.2.x86_64.qcow2 /var/lib/libvirt/images/provisioner.qcow2 120G
>
> virt-resize --expand /dev/vda1 /var/lib/libvirt/images/CentOS-8-GenericCloud-8.3.2011-20201204.2.x86_64.qcow2 /var/lib/libvirt/images/provisioner.qcow2
>
> virt-customize -a /var/lib/libvirt/images/provisioner.qcow2 --root-password password:Redhat01 --uninstall cloud-init --hostname provisioner.ocp4.example.local --run-command 'yum update -y'
>
> virt-customize -a /var/lib/libvirt/images/provisioner.qcow2 --selinux-relabel
>
> virt-install -q -n provisioner --vcpus 8 -r 16384 --os-type linux --os-variant rhel8.0 --disk path=/var/lib/libvirt/images/provisioner.qcow2,bus=scsi,size=120 --network bridge=provisioning,model=virtio,mac=52:54:00:67:2c:16 --network bridge=baremetal,model=virtio,mac=52:54:00:0d:7b:0c --cpu host-passthrough --graphics vnc --console pty,target_type=serial --noautoconsole --import


0 - load variables
> export LOCAL_SECRET_JSON="${HOME}/bundle-pullsecret.txt"
>
> export REGISTRY_FQDN="ip-10-0-3-10.testo.labo"
>
> export GODEBUG='x509ignoreCN=0'

- 1.0 - create redhat-operators catalog
>  oc adm catalog build --appregistry-org redhat-operators --from=registry.redhat.io/openshift4/ose-operator-registry:v${OCP_VERSION} --filter-by-os="linux/amd64" --registry-config=${HOME}/bundle-pullsecret.txt --to=${REGISTRY_FQDN}:5000/olm/redhat-operators:v1

- 1.1 - mirror certified-operators
> oc adm catalog mirror ${REGISTRY_FQDN}:5000/olm/redhat-operators:v1 ${REGISTRY_FQDN}:5000 --registry-config=${HOME}/bundle-pullsecret.txt --dry-run


- 2.0 - create certified-operators catalog
> oc adm catalog build --appregistry-org certified-operators --from=registry.redhat.io/openshift4/ose-operator-registry:v${OCP_VERSION} --filter-by-os="linux/amd64" --registry-config=${HOME}/bundle-pullsecret.txt --to=${REGISTRY_FQDN}:5000/olm/certified-operators:v1

- 2.1 - mirror certified-operators
> oc adm catalog mirror ${REGISTRY_FQDN}:5000/olm/certified-operators:v1 ${REGISTRY_FQDN}:5000 --registry-config=${HOME}/bundle-pullsecret.txt --dry-run


- 3.0 - create community-operators catalog
> oc adm catalog build --appregistry-org community-operators --from=registry.redhat.io/openshift4/ose-operator-registry:v${OCP_VERSION} --filter-by-os="linux/amd64" --registry-config=${HOME}/bundle-pullsecret.txt --to=${REGISTRY_FQDN}:5000/olm/community-operators:v1

- 3.1 - mirror community-operators
> oc adm catalog mirror ${REGISTRY_FQDN}:5000/olm/community-operators:v1 ${REGISTRY_FQDN}:5000 --registry-config=${HOME}/bundle-pullsecret.txt --dry-run

- 4 - copy specific operator
> skopeo copy docker://registry.access.redhat.com/ubi8/ubi-minimal@sha256:5cfbaf45ca96806917830c183e9f37df2e913b187aadb32e89fd83fa455ebaa6 docker://${REGISTRY_FQDN}:5000/example/ubi-minimal
