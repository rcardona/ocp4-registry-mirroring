- workaround

- check status of mirror daemon
> systemctl status mirror-registry
>

- update name of the container
> vim /etc/systemd/system/mirror-registry.service
>

> podman stop mirror-registry
>
> podman rm mirror-registry
>
> systemctl reload mirror-registry
>
> systemctl start mirror-registry
>

- discovering the repositoires in the registry
> curl -u <admin_user>:<admin_password> -X  GET https://${REGISTRY_FQDN}}:5000/v2/_catalog
>

- list existing images at local registry
> curl -u <admin_user>:<admin_password> https://${REGISTRY_FQDN}:5000/v2/ocp4/openshift4/tags/list | jq
>

- list existing images at local registry with specific number of lines
> curl -u <admin_user>:<admin_password> https://${REGISTRY_FQDN}:5000/v2/ocp4/openshift4/tags/list?n=1000 | jq
>

- pull the images from local registry
> podman pull ${REGISTRY_FQDN}:5000/ocp4/openshift4:4.3.0-kuryr-cni
>

- web resource of operator images (e.g 4.10)
> https://mirror.openshift.com/pub/openshift-v4/clients/ocp/4.10.10/release.txt
>

- Generate the base64-encoded user name and password or token for your mirror registry
> echo -n 'admin:admin' | base64 -w0
>
    YWRtaW46YWRtaW4=

 - Edit the install-config.yaml file to provide the additional information that is required for an installation in a restricted network. Update the pullSecret value to contain the authentication information for your registry

   pullSecret: '{"auths":{"bastion.ssalab.nl:5000": {"auth": "YWRtaW46YWRtaW4=","email": "you@example.com"}}}'

- create keys
> aws ec2 create-key-pair --key-name ocpkey --query 'KeyMaterial' --output text > ocpkey.pem
>
> chmod 400 ocpkey.pem
>
> ssh-keygen -y -f key.pem > key.pub
>


> curl -u admin:admin  https://${REGISTRY_FQDN}:5000/v2/_catalog
>

> curl -u admin:admin  https://${REGISTRY_FQDN}:5000/v2/ocp4-10-3/openshift4/tags/list?n=100 | jq
>

> curl -u admin:admin  https://${REGISTRY_FQDN}:5000/v2/ocp4-10-10/openshift4/tags/list?n=100 | jq
>
